package com.atlassian

import com.typesafe.scalalogging.StrictLogging
import io.gatling.core.session._
import io.gatling.core.validation.{Failure, Success}

/**
 * A collection of static methods to assist with various gatling operations
 */
package object gatling {

    object Utils extends StrictLogging {

        def store(key: String, value: Any): Expression[Session] = { session: Session =>
            Success(session.set(key, value))
        }

        def retrieve[T](key: String): Expression[T] = { session: Session =>
            session(key).asOption[T] match {
                case Some(value) => Success(value)
                case None => Failure("Unable to locate session variable with key: " + key)
            }
        }

        def retrieveOr[T](key: String, other: Expression[T]): Expression[T] = { session: Session =>
            session(key).asOption[T] match {
                case Some(value) => Success(value)
                case None => other(session)
            }
        }

        def trash(key: String): Expression[Session] = { session: Session =>
            Success(session.remove(key))
        }

        def dumpSession: Expression[Session] = { session =>
            logger.info(session.toString)
            Success(session)
        }
    }

}
