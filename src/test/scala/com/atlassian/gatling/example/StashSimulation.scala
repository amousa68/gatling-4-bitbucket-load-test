package com.atlassian.gatling.example

import com.atlassian.gatling.stash._
import com.atlassian.gatling.stash.modes.{GitCalls, RestCalls, WebCalls}
import io.gatling.core.Predef._
import io.gatling.core.scenario.Simulation

class StashSimulation extends Simulation with WebCalls with RestCalls with GitCalls {

    val webConfig = configureWeb("http://localhost:7990/stash", WebCalls.MOZILLA_5_0, WebCalls.LANGUAGE_ENGLISH)
    
    val reviewer = scenario("Reviewer Persona")
        .group("Create Project -> Repo -> User") {
            exec(Users.admin)
            .exec(Authentication.login)
            .exec(Projects.create)
            .exec(Repositories.create)
            .exec(Users.create)
            .exec(Authentication.logout)
        }
//        .exec(AdminOperations.newUser)
//        .exec(Repositories.owner(Users.current))
//        .exec(Authentication.login)
//        .exec(Authentication.logout)

    setUp(reviewer.inject(atOnceUsers(1)).protocols(webConfig))
    
}