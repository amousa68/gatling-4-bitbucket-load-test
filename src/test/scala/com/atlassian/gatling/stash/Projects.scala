package com.atlassian.gatling.stash

import com.atlassian.gatling.stash.types.Project
import io.gatling.core.Predef._
import io.gatling.core.session._
import io.gatling.core.structure._
import io.gatling.core.validation._
import io.gatling.http.Predef._
import scala.concurrent.duration._
import com.atlassian.gatling.stash._
import com.atlassian.gatling.stash.modes._
import scala.concurrent.duration.Duration._
import scala.util.Random
import org.apache.commons.lang3.RandomStringUtils

object Projects {

    val RE_ALT_TOKEN = """<input type="hidden" name="atl_token" value="([A-Za-z0-9]*)">"""

    def create(): ScenarioBuilder = create(Project.auto)

    def create(project: Project): ScenarioBuilder = {
        scenario("Create Project: " + project.key)
            .exec { session =>
                val projectKeys = session("__PROJECTS__").asOption[List[Project]] match {
                    case Some(existing) => existing ++ List(project)
                    case None => List(project)
                }
                session.set("__PROJECTS__", projectKeys)
                session.set("__CURRENT_PROJECT__", project.key)
            }
            .exec {
                http("new project page")
                    .get("/projects?create")
                    .check(regex(RE_ALT_TOKEN).find(0).saveAs("atl_token"))
            }
            .exec {
                http("project create form")
                    .post("/projects?create")
                    .formParam("atl_token", "${atl_token}")
                    .formParamMap(project.asMap())
                    .check(status.is(200))
                    .check(currentLocationRegex(""".*/projects/${__CURRENT_PROJECT__}$""").count.is(1))
            }.exec{
                _.remove("atl_token")
            }
    }

    def delete(): ScenarioBuilder = {
        scenario("Delete Last Project")
            .exec {
                http("project page")
                    .get("""/projects/${__CURRENT_PROJECT__}/settings""")
                    .check(status.is(200))
            }
            .exec {
                http("project delete ajax")
                    .delete("/projects/${__CURRENT_PROJECT__}")
                    .header(HttpHeaderNames.Accept, HttpHeaderValues.ApplicationJson)
                    .header(HttpHeaderNames.ContentType, HttpHeaderValues.ApplicationJson)
                    .check(status.is(204))
            }
            .exec {
                http("project list page")
                    .get("/projects")
                    .check(status.is(200))
            }
            .exec { session =>
                val projectKey = session("__CURRENT_PROJECT__").as[String]
                val projectKeys = session("__PROJECTS__").asOption[List[Project]] match {
                    case Some(existing) => existing.dropWhile { _.key.equalsIgnoreCase(projectKey) }
                    case None => List()
                }
                session.set("__PROJECTS__", projectKeys)
                session.remove("__CURRENT_PROJECT__")
            }
    }

}