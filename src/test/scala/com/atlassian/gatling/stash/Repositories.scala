package com.atlassian.gatling.stash

import com.atlassian.gatling.stash._
import com.atlassian.gatling.stash.types._
import io.gatling.core.Predef._
import io.gatling.core.session._
import io.gatling.core.structure._
import io.gatling.http.Predef._
import org.apache.commons.lang3.RandomStringUtils
import scala.concurrent.duration._

object Repositories {


    val RE_ALT_TOKEN = """<input type="hidden" name="atl_token" value="([A-Za-z0-9]*)">"""

    def create(): ScenarioBuilder = {
        scenario("Create Repository")
            .exec {
                create(Repository.auto)
            }
    }

    def create(repository: Repository): ScenarioBuilder = {
        scenario("Create Repository: " + repository.name)
            .exec { session =>
                val repositories = session("__REPOSITORIES__").asOption[List[Repository]] match {
                    case Some(existing) => existing ++ List(repository)
                    case None => List(repository)
                }
                session.set("__REPOSITORIES__", repositories)
                session.set("__CURRENT_REPOSITORY__", repository.name.toLowerCase())
            }
            .exec {
                http("new repository page")
                    .get("/projects/${__CURRENT_PROJECT__}/repos?create")
                    .check(regex(RE_ALT_TOKEN).find(0).saveAs("atl_token"))
            }
            .exec {
                http("repo create form")
                    .post("/projects/${__CURRENT_PROJECT__}/repos?create")
                    .formParam("atl_token", "${atl_token}")
                    .formParamMap(repository.asMap())
                    .check(status.is(200))
                    .check(currentLocationRegex(".*/projects/${__CURRENT_PROJECT__}/repos/" +
                        repository.name.toLowerCase() + "/browse$").count.is(1))
            }.exec {
                _.remove("atl_token")
            }
    }

    def delete(): ScenarioBuilder = {
        scenario("Delete Last Repository")
            .exec {
                http("repository page")
                    .get("""/projects/${__CURRENT_PROJECT__}/repos/${__CURRENT_REPOSITORY__}/settings""")
                    .check(status.is(200))
            }
            .exec {
                http("repository delete ajax")
                    .delete("/projects/${__CURRENT_PROJECT__}/repos/${__CURRENT_REPOSITORY__}")
                    .header(HttpHeaderNames.Accept, HttpHeaderValues.ApplicationJson)
                    .header(HttpHeaderNames.ContentType, HttpHeaderValues.ApplicationJson)
                    .check(status.in(202, 204))
            }
            .exec {
                http("project page")
                    .get("/projects/${__CURRENT_PROJECT__}")
                    .check(status.is(200))
            }
            .exec { session =>
                val repositoryName = session("__CURRENT_REPOSITORY__").as[String]
                val repositories = session("__REPOSITORIES__").asOption[List[Repository]] match {
                    case Some(existing) => existing.dropWhile { _.name.equalsIgnoreCase(repositoryName) }
                    case None => List()
                }
                session.set("__REPOSITORIES__", repositories)
                session.remove("__CURRENT_REPOSITORY__")
            }
    }

}