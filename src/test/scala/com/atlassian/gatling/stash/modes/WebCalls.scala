package com.atlassian.gatling.stash.modes

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import scala.concurrent.duration._
import io.gatling.http.config.HttpProtocolBuilder
import io.gatling.http.HeaderNames
import io.gatling.http.HeaderValues

object WebCalls {

    val MOZILLA_5_0 = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.8; rv:16.0) Gecko/20100101 Firefox/16.0"

    val LANGUAGE_ENGLISH = "en-US,en;q=0.5"
    val LANGUAGE_JAPANESE = "ja-JP,ja;q=0.5"
    val LANGUAGE_GERMAN = "de-DE,de;q=0.5"
    val LANGUAGE_FRENCH = "fr-FR,fr;q=0.5"

    val DEFAULT_ACCEPT_HEADER = "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8"

    val english = Map(HeaderNames.AcceptLanguage -> WebCalls.LANGUAGE_ENGLISH)
    val japanese = Map(HeaderNames.AcceptLanguage -> WebCalls.LANGUAGE_JAPANESE)
    val german= Map(HeaderNames.AcceptLanguage -> WebCalls.LANGUAGE_GERMAN)
    val french = Map(HeaderNames.AcceptLanguage -> WebCalls.LANGUAGE_FRENCH)

    val json = Map(HeaderNames.ContentType -> HeaderValues.ApplicationJson)
    val html = Map(HeaderNames.ContentType -> HeaderValues.TextHtml)

}

trait WebCalls {

    def configureWeb(
        baseUrl: String = "http://localhost:7990/stash",
        userAgent: String = WebCalls.MOZILLA_5_0,
        language: String = WebCalls.LANGUAGE_ENGLISH): HttpProtocolBuilder = {
        http
            .baseURL(baseUrl)
            .acceptHeader(WebCalls.DEFAULT_ACCEPT_HEADER)
            .doNotTrackHeader("1")
            .acceptLanguageHeader(language)
            .acceptEncodingHeader("gzip, deflate")
            .userAgentHeader(userAgent)
    }

}