package com.atlassian.gatling.stash.types

import org.apache.commons.lang3.RandomStringUtils

case class Repository(name: String, scmId: String = "git", forkable: Boolean = true) {
    def asMap(): Map[String, String] = {
        Map("name" -> name, "scmId" -> scmId, "forkable" -> forkable.toString)
    }
}

object Repository {
    def auto: Repository = {
        new Repository(
            RandomStringUtils.randomAlphabetic(20))
    }
}