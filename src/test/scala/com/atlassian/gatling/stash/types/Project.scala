package com.atlassian.gatling.stash.types

import org.apache.commons.lang3.RandomStringUtils

case class Project(
        name: String, 
        key: String, 
        description: String) {
    def asMap(): Map[String, String] = {
        Map("name" -> name, "key" -> key, "description" -> description)
    }
}

object Project {
    
    val NULL = Project("NA", "NA", "NA")
    
    def auto: Project = {
        new Project(
            RandomStringUtils.randomAlphabetic(10),
            RandomStringUtils.randomAlphabetic(5).toUpperCase() + RandomStringUtils.randomNumeric(5),
            (1.to(5)).map(num => {
                RandomStringUtils.randomAlphabetic(5)
            }).mkString(" "))
    }
}