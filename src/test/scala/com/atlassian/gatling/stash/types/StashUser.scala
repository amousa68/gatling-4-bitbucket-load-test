package com.atlassian.gatling.stash.types

import org.apache.commons.lang3.RandomStringUtils

case class StashUser(username: String,
                     password: String,
                     fullname: String = "Uncle Specified",
                     email: String = "unspecified@email.com") {

    def asMap(): Map[String, String] = {
        Map("username" -> username,
            "password" -> password,
            "fullname" -> fullname,
            "email" -> email,
            "password" -> password,
            "confirmPassword" -> password)
    }
}

object StashUser {

    def random: StashUser =
        StashUser(RandomStringUtils.randomAlphabetic(20), RandomStringUtils.randomNumeric(10))

//    def unapply(user: StashUser): Map[String, String] = {
//        Map("username" -> user.username,
//            "password" -> user.password,
//            "fullname" -> user.fullname,
//            "email" -> user.email,
//            "password" -> user.password,
//            "confirmPassword" -> user.password)
//    }
}