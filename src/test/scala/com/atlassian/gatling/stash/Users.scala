package com.atlassian.gatling.stash

import com.atlassian.gatling._
import com.atlassian.gatling.stash.types.StashUser
import com.typesafe.scalalogging.StrictLogging
import io.gatling.core.Predef._
import io.gatling.core.session._
import io.gatling.core.validation._
import io.gatling.http.Predef._
import org.apache.commons.lang3.RandomStringUtils

object Users extends StrictLogging {

    val LICENSES_REGEX = "There are <b>([0-9]*)</b> users remaining on your <b>([0-9]*)</b> user license"

    def streamOfRandom = Iterator.continually {
        StashUser(RandomStringUtils.randomAlphabetic(20), RandomStringUtils.randomNumeric(10))
    }

    def current: Expression[StashUser] = _("CURRENT_USER").as[StashUser]
    def currentUsername: Expression[String] = _("CURRENT_USER").as[StashUser].username
    def currentPassword: Expression[String] = _("CURRENT_USER").as[StashUser].password

    def admin: Expression[Session] = _.set("CURRENT_USER", StashUser("admin", "admin"))
    def random: Expression[Session] = _.set("CURRENT_USER", StashUser.random)

    def create = exec {
        http("admin page").get("/admin")
    }.exec {
        http("admin users page").get("/admin/users")
    }.exec {
        http("create user").get("/admin/users?create").check(checkLicense.saveAs("licenses"))
    }.exec { session =>
        session.set("formdata", session("CURRENT_USER").as[StashUser].asMap)
    }.doIf(hasFreeLicense) {
        exec {
            http("create user form")
                .post("/admin/users?create")
                .formParamMap("${formdata}")
                .asFormUrlEncoded
        }
    }.exec(Utils.trash("licenses")).exec(Utils.trash("formdata"))

    private def hasFreeLicense: Expression[Boolean] = session => {
            session ("licenses").asOption[(String, String)] match {
            case Some((users, total)) =>
                if (users.toInt > 0) {
                    Success(true)
                } else {
                    Failure("Not enough free users to continue")
                }
            case None =>
                Failure("Regex for licenses did not match anything")
        }
    }

    private def checkLicense = regex(LICENSES_REGEX).ofType[(String, String)]
}