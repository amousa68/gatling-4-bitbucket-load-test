package com.atlassian.gatling.stash

import io.gatling.core.Predef._
import io.gatling.core.structure.ChainBuilder

object AdminOperations {

    def newUser: ChainBuilder = exec(Users.admin)
        .exec(Authentication.login)
        .exec(Users.create)
        .exec(Authentication.logout)

    def newProject: ChainBuilder = exec(Users.admin)
        .exec(Authentication.login)
        .exec(Projects.create)

    def newRepository: ChainBuilder = exec(Users.admin)
        .exec(Authentication.login)
        .exec(Repositories.create())
}